Feature: Add spotlight widget
  In order to promote content
  As a site administrator
  I need to be able to add a spotlight

  Background:
    Given I am logged in as a user with the "administrator" role
      And I am viewing a landing page
    When I click "Layout"
    And I click "Add block in Section 1, Content region"
    And I click "Add Spotlight"

  @api @javascript @panopoly_widgets @panopoly2 @panopoly_spotlight
  Scenario: Add a spotlight
    When I fill in the following:
      | Title                                  | Spotlight           |
      | Slide Duration                         | 3                   |
      | settings[block_form][field_panopoly_spotlight_slides][0][subform][field_panopoly_spotlight_title][0][value] | Testing item title  |
      | Link                                   | http://drupal.org   |
      | Caption                                | Testing description |
      And I uncheck the box "settings[label_display]"
      And I press "Browse media"
      And I wait for AJAX to finish
      And I switch to the frame "entity_browser_iframe_panopoly_media_field_media_browser"
      And I click "Upload images"
      And I wait for AJAX to finish
      And I drop the file "test-sm.png" to "edit-upload"
      And I wait for AJAX to finish
      # Second AJAX for media entity form. Timing is off in iframes for some reason.
      And I wait for AJAX to finish
      And I fill in "Alternative text" with "Testing alt text"
      And I press "Select"
    When I switch out of all frames
    And I wait for AJAX to finish
    And I press "Save" in the "Dialog buttons" region
      And I press "Save layout"
    Then I should see "Testing description"
      And I should see "Testing item title"
      # Per an old bug described in issue #2075903
      And I should not see "Spotlight"

  @api @javascript @panopoly_widgets @panopoly_spotlight
  Scenario: Image is required per issue #2075903
    When I fill in the following:
      | Slide Duration                             | 3                   |
      | settings[block_form][field_panopoly_spotlight_slides][0][subform][field_panopoly_spotlight_title][0][value] | Testing item title  |
      | Link                                       | http://drupal.org   |
#      | Caption                                | Testing description |
      And I press "Save" in the "Dialog buttons" region
    Then I should see "Image field is required"
