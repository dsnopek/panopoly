<?php

/**
 * @file
 * The main Behat context.
 */

use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Define application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  /**
   * Initializes context.
   */
  public function __construct() {
    // Initialize your context here.
  }

}
