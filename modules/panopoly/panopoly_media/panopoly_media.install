<?php

/**
 * @file
 * Install hooks for Panopoly Media.
 */

use Drupal\Core\Config\Entity\ConfigEntityStorageInterface;
use Drupal\Core\Config\FileStorage;
use Drupal\panopoly_media\Update\ContentModelUpdater;
use Drupal\system\Entity\Action;

/**
 * Implements hook_install().
 */
function panopoly_media_install() {
  _panopoly_media_set_delete_config();
}

/**
 * Implements hook_update_dependencies().
 */
function panopoly_media_update_dependencies() {
  $dependencies = [];
  $dependencies['panopoly_media'][8209] = [
    'panopoly_images' => 8203,
  ];
  return $dependencies;
}

/**
 * Adds the configuration entity for the file delete action.
 */
function _panopoly_media_set_delete_config() {
  if ($action = Action::load('panopoly_media_file_delete_action')) {
    return;
  }

  $action = Action::create([
    'langcode' => 'en',
    'status' => TRUE,
    'dependencies' => [
      'module' => [
        'file',
        'panopoly_media',
      ],
    ],
    'id' => 'panopoly_media_file_delete_action',
    'label' => 'Delete file',
    'type' => 'file',
    'plugin' => 'panopoly_media_file_delete_action',
    "configuration" => [],
  ]);
  $action->save();
}

/**
 * Set WYSIWYG embed button icon.
 */
function panopoly_media_update_8201() {
  // We no longer set the embed button in this way.
}

/**
 * Enable Inline Entity Form dependency.
 */
function panopoly_media_update_8202() {
  \Drupal::service('module_installer')->install(['inline_entity_form']);
}

/**
 * Add file delete action configuration.
 */
function panopoly_media_update_8203() {
  _panopoly_media_set_delete_config();
}

/**
 * Switch embed display modes to use responsive image styles.
 */
function panopoly_media_update_8204() {
  $options = [
    'media.image.embed_large' => [
      'type' => 'image',
      'settings' => [
        'image_style' => '',
      ],
    ],
    'media.image.embed_medium' => [
      'type' => 'responsive_image',
      'settings' => [
        'responsive_image_style' => 'panopoly_images_quarter',
      ],
    ],
    'media.image.embed_small' => [
      'type' => 'responsive_image',
      'settings' => [
        'responsive_image_style' => 'panopoly_images_thumbnail',
      ],
    ],
  ];

  /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface[] $displays */
  $displays = Drupal::entityTypeManager()->getStorage('entity_view_display')->loadMultiple(array_keys($options));
  foreach ($displays as $display) {
    $display->setComponent('field_media_image', $options[$display->id()])
      ->save();
  }
}

/**
 * Media content model conversion.
 */
function panopoly_media_update_8205(&$context) {
  $updater = new ContentModelUpdater();

  // Set state first time through.
  if (empty($context['sandbox']['state'])) {
    $context['sandbox']['state'] = 'init';
  }

  $context['#finished'] = 0;

  // Update data.
  switch ($context['sandbox']['state']) {
    case 'init':
      $context['message'] = 'Updating config';
      $updater->init();
      $context['sandbox']['state'] = 'convert_fields';
      break;

    case 'convert_fields':
      $context['message'] = 'Converting media reference fields';
      $updater->convertFields();
      $context['sandbox']['state'] = 'convert_entities';
      break;

    case 'convert_entities':
      $context['message'] = 'Converting media entities';
      $context['#finished'] = $updater->convertMedia($context);
      // Clean up when done.
      if ($context['#finished'] == 1) {
        $updater->cleanup();
      }
      break;
  }
}

/**
 * Update config to use our custom entity browser view.
 */
function panopoly_media_update_8206() {
  /** @var \Drupal\Core\Extension\ExtensionList $module_list */
  $module_list = \Drupal::service('extension.list.module');

  // Import new entity browser view.
  $view_config_name = 'views.view.panopoly_media_browser';
  $config_path = $module_list->getPath('panopoly_media') . '/config/install';
  $source = new FileStorage($config_path);
  /** @var \Drupal\Core\Config\StorageInterface $config_storage */
  $config_storage = \Drupal::service('config.storage');
  $config_storage->write($view_config_name, $source->read($view_config_name));

  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::service('config.factory');

  // Update our two entity browsers to use it.
  $media_browsers_to_update = [
    'entity_browser.browser.panopoly_media_field_media_browser' => '828212e6-5506-4f23-a333-4b56a08ac85b',
    'entity_browser.browser.panopoly_media_wysiwyg_media_browser' => 'a1ed5dca-2adc-478d-a9b0-014a7df6bb6e',
  ];
  foreach ($media_browsers_to_update as $config_name => $widget_uuid) {
    $config = $config_factory->getEditable($config_name);
    // Only update this value if it hasn't been changed by the site admin.
    if ($config->get("widgets.{$widget_uuid}.settings.view") === 'media_entity_browser') {
      $config->set("widgets.{$widget_uuid}.settings.view", 'panopoly_media_browser');
      $config->save();
    }
  }

  // Update the media entity browser thumbnail image style.
  $config = $config_factory->getEditable('image.style.panopoly_media_entity_browser_thumbnail');
  $data = $config->getRawData();
  if ($data['label'] === 'Entity Browser Thumbnail') {
    $data['label'] = 'Media Entity Browser Thumbnail';
  }
  if (isset($data['effects']['384c8fe8-d61d-42df-a0ac-f006a5f9b232'])) {
    unset($data['effects']['384c8fe8-d61d-42df-a0ac-f006a5f9b232']);
    $data['effects']['793bbd62-5859-4049-8b02-967dd91084c4'] = [
      'uuid' => '793bbd62-5859-4049-8b02-967dd91084c4',
      'id' => 'image_scale_and_crop',
      'weight' => 1,
      'data' => [
        'width' => 350,
        'height' => 200,
        'anchor' => 'center-center',
      ],
    ];
  }
  $config->setData($data);
  $config->save();
}

/**
 * Add missing embed displays for File and Remote video.
 */
function panopoly_media_update_8207() {
  /** @var \Drupal\Core\Extension\ExtensionList $module_list */
  $module_list = \Drupal::service('extension.list.module');

  $new_config_entities = [
    'core.entity_view_display.media.panopoly_media_file.embed_medium',
    'core.entity_view_display.media.panopoly_media_file.embed_small',
    'core.entity_view_display.media.panopoly_media_remote_video.embed_medium',
    'core.entity_view_display.media.panopoly_media_remote_video.embed_small',
  ];
  foreach ($new_config_entities as $config_name) {
    $config_path = $module_list->getPath('panopoly_media') . '/config/install';
    $source = new FileStorage($config_path);
    /** @var \Drupal\Core\Config\StorageInterface $config_storage */
    $config_storage = \Drupal::service('config.storage');
    $config_storage->write($config_name, $source->read($config_name));
  }

}

/**
 * Add video type block.
 */
function panopoly_media_update_8208() {
  /** @var \Drupal\Core\Extension\ExtensionList $module_list */
  $module_list = \Drupal::service('extension.list.module');

  $new_config_entities = [
    'block_content.type.panopoly_media_video',
    'entity_browser.browser.panopoly_media_field_video_browser',
    'field.storage.block_content.field_panopoly_media_video_video',
    'field.field.block_content.panopoly_media_video.field_panopoly_media_video_video',
    'core.entity_form_display.block_content.panopoly_media_video.default',
    'core.entity_view_display.block_content.panopoly_media_video.default',
  ];
  /** @var \Drupal\Core\Config\StorageInterface $config_storage */
  $config_storage = \Drupal::service('config.storage');
  /** @var \Drupal\Core\Config\ConfigManagerInterface $config_manager */
  $config_manager = \Drupal::service('config.manager');
  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
  $entity_type_manager = \Drupal::entityTypeManager();

  foreach ($new_config_entities as $config_name) {
    if ($config_storage->read($config_name)) {
      // If this config already exists, skip it.
      continue;
    }

    $config_path = $module_list->getPath('panopoly_media') . '/config/install';
    $source = new FileStorage($config_path);
    $config_data = $source->read($config_name);

    $type = $config_manager->getEntityTypeIdByName($config_name);
    $entity_storage = $entity_type_manager->getStorage($type);
    if ($entity_storage instanceof ConfigEntityStorageInterface) {
      $entity = $entity_storage->createFromStorageRecord($config_data);
      $entity->save();
    }
  }
}

/**
 * Add manual crop.
 */
function panopoly_media_update_8209() {
  /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
  $config_factory = \Drupal::service('config.factory');

  // Update 'core.entity_view_display.media.panopoly_media_image.embed_large'.
  $config = $config_factory->getEditable('core.entity_view_display.media.panopoly_media_image.embed_large');
  $data = $config->getRawData();
  $data['content']['field_media_image']['settings']['image_style'] = 'panopoly_images_original';
  $data['dependencies']['config'][] = 'image.style.panopoly_images_original';
  sort($data['dependencies']['config']);
  $config->setData($data);
  $config->save();

  // Update 'image.style.panopoly_media_entity_browser_thumbnail'.
  $config = $config_factory->getEditable('image.style.panopoly_media_entity_browser_thumbnail');
  $data = $config->getRawData();
  $data['effects']['904a4507-5041-4180-9a87-2c2275adcbb7'] = [
    'uuid' => '904a4507-5041-4180-9a87-2c2275adcbb7',
    'id' => 'crop_crop',
    'weight' => -10,
    'data' => [
      'crop_type' => 'panopoly_images_default',
    ],
  ];
  $data['dependencies']['config'][] = 'crop.type.panopoly_images_default';
  $data['dependencies']['module'][] = 'crop';
  $config->setData($data);
  $config->save();

  /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
  $entity_type_manager = \Drupal::entityTypeManager();
  /** @var \Drupal\Core\Entity\EntityStorageInterface $entity_form_display_storage */
  $entity_form_display_storage = $entity_type_manager->getStorage('entity_form_display');

  // Update the form widgets.
  $displays_to_update = [
    'media.panopoly_media_image.default',
    'media.panopoly_media_image.entity_browser',
  ];
  foreach ($displays_to_update as $display_id) {
    /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $display */
    $display = $entity_form_display_storage->load($display_id);
    $component = $display->getComponent('field_media_image');
    if ($component['type'] === 'image_image') {
      $component['type'] = 'image_widget_crop';
      $component['settings'] = [
        'show_default_crop' => TRUE,
        'warn_multiple_usages' => TRUE,
        'preview_image_style' => 'panopoly_images_thumbnail',
        'crop_list' => [
          'panopoly_images_default',
        ],
        'progress_indicator' => 'throbber',
        'crop_types_required' => [],
        'show_crop_area' => FALSE,
      ];

      $display->setComponent('field_media_image', $component);
      $display->save();
    }
  }
}

/**
 * Add video type block again - with the new name.
 */
function panopoly_media_update_8210() {
  return panopoly_media_update_8208();
}
