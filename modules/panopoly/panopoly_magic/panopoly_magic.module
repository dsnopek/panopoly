<?php

/**
 * @file
 * Hook implementations for panopoly_magic.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextDefinition;
use Drupal\ctools_views\Plugin\Display\Block as CtoolsBlockDisplay;
use Drupal\panopoly_magic\Plugin\Block\MagicViewsBlock;
use Drupal\panopoly_magic\Plugin\views\display\MagicBlock;

/**
 * Implements hook_block_alter().
 */
function panopoly_magic_block_alter(&$definitions) {
  foreach ($definitions as $plugin_id => &$definition) {
    // There's a couple of core blocks that don't specify a category and end up
    // with 'core'. Let's move them to 'System' instead.
    if ($definition['category'] === 'core') {
      $definition['category'] = t('System');
    }

    // We need to modify Views blocks to add contexts configured on MagicBlock.
    if (strpos($plugin_id, 'views_block:') === 0) {
      [, $derivative_id] = explode(':', $plugin_id, 2);
      _panopoly_magic_views_block_alter($derivative_id, $definition);
    }
  }
}

/**
 * Modifies derivatives of 'views_block' to add configured contexts.
 *
 * @param string $delta
 *   The view display delta.
 * @param array &$derivative
 *   The derivative plugin definition.
 */
function _panopoly_magic_views_block_alter($delta, array &$derivative) {
  [$view_id, $display_id] = explode('-', $delta, 2);

  /** @var ViewEntityInterface $view */
  $view = \Drupal::entityTypeManager()->getStorage('view')->load($view_id);
  $executable = $view->getExecutable();
  $executable->initDisplay();

  $display = $executable->displayHandlers->get($display_id);

  // Replace context definitions, if they are configured.
  $magic_arguments = $display->getOption('magic_arguments') ?: [];
  foreach ($display->getHandlers('argument') as $argument_name => $argument_handler) {
    if (!isset($magic_arguments[$argument_name])) {
      continue;
    }
    $argument_options = $magic_arguments[$argument_name];
    if ($argument_options['type'] === 'context') {
      $derivative['context_definitions'][$argument_name] = ContextDefinition::create($argument_options['context'])
        ->setLabel($argument_handler->adminLabel())
        ->setRequired(!$argument_options['context_optional']);
    }
  }

  // Change the class for the block.
  $derivative['class'] = MagicViewsBlock::class;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function panopoly_magic_form_layout_builder_add_block_alter(array &$form, FormStateInterface $form_state, $form_id) {
  _panopoly_magic_simplify_layout_builder_block_form($form, $form_state, $form_id);
  \Drupal::service('panopoly_magic.alterations.reusable_blocks')->alterForm($form, $form_state, $form_id);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function panopoly_magic_form_layout_builder_update_block_alter(array &$form, FormStateInterface $form_state, $form_id) {
  _panopoly_magic_simplify_layout_builder_block_form($form, $form_state, $form_id);
  \Drupal::service('panopoly_magic.alterations.reusable_blocks')->alterForm($form, $form_state, $form_id);
}

/**
 * Simplifies the layout builder add and update block forms.
 *
 * @param array $form
 *   The form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 * @param string $form_id
 *   The form id.
 */
function _panopoly_magic_simplify_layout_builder_block_form(array &$form, FormStateInterface $form_state, $form_id) {
  $form['settings']['admin_label']['#access'] = FALSE;
}

/**
 * Implements hook_views_plugins_display_alter().
 */
function panopoly_magic_views_plugins_display_alter(&$displays) {
  if (!empty($displays['block']['class']) && $displays['block']['class'] == CtoolsBlockDisplay::class) {
    $displays['block']['class'] = MagicBlock::class;
  }
}

/**
 * Implements hook_config_schema_info_alter().
 */
function panopoly_magic_config_schema_info_alter(&$definitions) {
  // Views block.
  $definitions['views_block']['mapping']['view_settings'] = [
    'type' => 'string',
    'label' => 'Display type',
  ];
  $definitions['views_block']['mapping']['header_type'] = [
    'type' => 'string',
    'label' => 'Column header',
  ];
  $definitions['views_block']['mapping']['view_mode'] = [
    'type' => 'string',
    'label' => 'Column header',
  ];
  $definitions['views_block']['mapping']['use_pager'] = [
    'type' => 'boolean',
    'label' => 'Use pager',
  ];
  $definitions['views_block']['mapping']['exposed_form'] = [
    'type' => 'mapping',
    'label' => 'Saved exposed form values',
    'mapping' => [],
  ];
  $definitions['views_block']['mapping']['exposed_form']['mapping']['filters'] = [
    'type' => 'sequence',
    'label' => 'Filters',
  ];
  $definitions['views_block']['mapping']['exposed_form']['mapping']['sort'] = [
    'type' => 'mapping',
    'label' => 'Sort',
    'mapping' => [],
  ];
  $definitions['views_block']['mapping']['exposed_form']['mapping']['sort']['mapping']['sort_by'] = [
    'type' => 'string',
    'label' => 'Sort by',
  ];
  $definitions['views_block']['mapping']['exposed_form']['mapping']['sort']['mapping']['sort_order'] = [
    'type' => 'string',
    'label' => 'Sort order',
  ];

  // Views block display.
  $definitions['views.display.block']['mapping']['allow']['mapping']['display_type'] = [
    'type' => 'string',
    'label' => 'Display type',
  ];
  $definitions['views.display.block']['mapping']['allow']['mapping']['exposed_form'] = [
    'type' => 'string',
    'label' => 'Use exposed form as block configuration',
  ];
  $definitions['views.display.block']['mapping']['allow']['mapping']['use_pager'] = [
    'type' => 'string',
    'label' => 'Use pager',
  ];
}

/**
 * Implements hook_theme().
 */
function panopoly_magic_theme() {
  return [
    'panopoly_magic_preview' => [
      'render element' => 'preview',
    ],
    'panopoly_magic_choose_block_link' => [
      'variables' => [
        'link' => '',
        'description' => '',
        'add_link' => '',
      ],
    ],
  ];
}

/**
 * Default preprocess hook for panopoly-magic-theme.html.twig template.
 */
function template_preprocess_panopoly_magic_preview(&$vars) {
  $preview =& $vars['preview'];
  $vars['title'] = $preview['#title'];
  if (!empty($preview['add_link'])) {
    $vars['add_link'] = $preview['add_link'];
    unset($preview['add_link']);
  }
}
