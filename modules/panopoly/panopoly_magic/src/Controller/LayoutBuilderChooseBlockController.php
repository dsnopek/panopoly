<?php

namespace Drupal\panopoly_magic\Controller;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\panopoly_magic\BlockPreviewRenderer;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Defines a controller to choose a new block.
 *
 * @internal
 *   Controller classes are internal.
 */
class LayoutBuilderChooseBlockController implements ContainerInjectionInterface {

  use AjaxHelperTrait;
  use LayoutBuilderContextTrait;
  use LayoutBuilderHighlightTrait;
  use StringTranslationTrait;

  /**
   * The block manager.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Panopoly Magic config object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The Panopoly block preview renderer.
   *
   * @var \Drupal\panopoly_magic\BlockPreviewRenderer
   */
  protected $blockPreviewRenderer;

  /**
   * ChooseBlockController constructor.
   *
   * @param \Drupal\Core\Block\BlockManagerInterface $block_manager
   *   The block manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The Panopoly Magic config object.
   * @param \Drupal\panopoly_magic\BlockPreviewRenderer $block_preview_renderer
   *   The block preview renderer service.
   */
  public function __construct(BlockManagerInterface $block_manager, EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, ImmutableConfig $config, BlockPreviewRenderer $block_preview_renderer) {
    $this->blockManager = $block_manager;
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->config = $config;
    $this->blockPreviewRenderer = $block_preview_renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('config.factory')->get('panopoly_magic.settings'),
      $container->get('panopoly_magic.block_preview_renderer')
    );
  }

  /**
   * Provides the UI for choosing a new block.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  public function chooseBlock(Request $request, SectionStorageInterface $section_storage, int $delta, $region) {
    $current_category = $request->query->get('category');
    $preview_mode = $this->config->get('choose_block_preview');

    $block_categories['#type'] = 'container';
    $block_categories['#attributes']['class'][] = 'panopoly-magic-choose-block';
    $block_categories['#attributes']['class'][] = 'panopoly-magic-choose-block-preview-' . $preview_mode;
    if ($this->config->get('choose_block_descriptions')) {
      $block_categories['#attributes']['class'][] = 'panopoly-magic-choose-block-use-descriptions';
    }
    $block_categories['#attributes']['data-layout-builder-target-highlight-id'] = $this->blockAddHighlightId($delta, $region);

    $definitions = $this->blockManager->getFilteredDefinitions('layout_builder', $this->getPopulatedContexts($section_storage), [
      'section_storage' => $section_storage,
      'delta' => $delta,
      'region' => $region,
      'list' => 'inline_blocks',
    ]);
    $grouped_definitions = $this->blockManager->getGroupedDefinitions($definitions);

    $custom_blocks_key = (string) $this->t('Custom');
    $custom_blocks = [];
    if (isset($grouped_definitions[$custom_blocks_key])) {
      $custom_blocks = $grouped_definitions[$custom_blocks_key];
      unset($grouped_definitions[$custom_blocks_key]);
    }

    $block_categories['categories'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['panopoly-magic-choose-block-categories'],
      ],
    ];
    $block_categories['categories']['categories'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [],
    ];
    foreach (array_keys($grouped_definitions) as $category) {
      $category_element = [
        '#type' => 'link',
        '#url' => Url::fromRoute('layout_builder.choose_block',
          [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'category' => $category,
          ]
        ),
        '#title' => $category,
        '#attributes' => $this->getAjaxAttributes(),
        '#wrapper_attributes' => [
          'class' => ['panopoly-magic-choose-block-category'],
        ],
      ];
      if ($current_category === $category) {
        $category_element['#wrapper_attributes']['class'][] = 'panopoly-magic-choose-block-current-category';
      }
      $block_categories['categories']['categories']['#items'][] = $category_element;
    }

    if (!empty($custom_blocks)) {
      $block_categories['categories']['custom'] = [
        '#theme' => 'item_list',
        '#list_type' => 'ul',
        '#items' => [],
      ];
      foreach ($custom_blocks as $block_id => $block) {
        $block_categories['categories']['custom']['#items'][] = [
          '#type' => 'link',
          '#url' => Url::fromRoute('layout_builder.add_block', [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $block_id,
          ]),
          '#title' => $this->t('Add @name', ['@name' => $block['admin_label']]),
          '#attributes' => $this->getAjaxAttributes(),
          '#wrapper_attributes' => [
            'class' => ['panopoly-magic-choose-block-custom'],
          ],
        ];
      }
    }

    $block_categories['blocks'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['panopoly-magic-choose-block-list'],
      ],
    ];

    if ($current_category && !empty($grouped_definitions[$current_category])) {
      $block_categories['blocks']['list'] = $this->getBlockLinks($section_storage, $delta, $region, $grouped_definitions[$current_category]);

      if ($preview_mode === 'single') {
        $block_categories['blocks']['preview'] = [
          '#type' => 'container',
          '#attached' => [
            'library' => ['panopoly_magic/preview.base'],
          ],
          '#attributes' => [
            'class' => ['panopoly-magic-single-preview-wrapper'],
          ],
          '#weight' => -100,
        ];
        $block_categories['blocks']['preview']['preview'] = [
          '#theme' => 'panopoly_magic_preview',
          '#title' => $this->t("Select a block to show its preview"),
          '#attributes' => [
            'id' => 'panopoly-magic-single-preview',
          ],
        ];
        $block_categories['blocks']['preview']['preview']['content'] = [
          '#markup' => '<p>' . $this->t("No preview") . '</p>',
        ];
      }
    }
    else {
      $block_categories['blocks']['list'] = [
        '#markup' => '<p class="panopoly-magic-choose-block-description">' . $this->t("Please select a category from the left.") . '</p>',
      ];
    }

    $build['block_categories'] = $block_categories;
    $build['#attached']['library'][] = 'panopoly_magic/preview.choose_block_or_layout';
    $build['#cache'] = [
      'contexts' => [
        'languages',
        'theme',
        'url.path',
        'url.query_args',
        'user.permissions',
      ],
    ];
    return $build;
  }

  /**
   * Provides the UI for choosing a new block.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   * @param string $plugin_id
   *   The block plugin id.
   *
   * @return array
   *   A render array.
   */
  public function previewBlock(SectionStorageInterface $section_storage, int $delta, $region, $plugin_id) {
    $preview_mode = $this->config->get('choose_block_preview');
    if ($preview_mode === 'single') {
      $target_selector = '#panopoly-magic-single-preview';
    }
    elseif ($preview_mode === 'manual') {
      $target_selector = '#panopoly-magic-manual-preview-' . Html::getClass($plugin_id);
    }
    else {
      return new Response('Not found.', 404);
    }

    $contexts = $this->getPopulatedContexts($section_storage);
    $content = $this->blockPreviewRenderer->buildBlockPreview($plugin_id, $contexts);

    try {
      $block_definition = $this->blockManager->getDefinition($plugin_id);
      $block_admin_label = $block_definition['admin_label'];
    }
    catch (PluginNotFoundException $e) {
      $block_admin_label = $this->t("Preview");
    }

    $wrapper = [
      '#theme' => 'panopoly_magic_preview',
      '#title' => $block_admin_label,
      'add_link' => $this->getBlockAddLink($section_storage, $delta, $region, $plugin_id, $block_admin_label),
      'content' => $content,
    ];

    if ($preview_mode === 'single') {
      $wrapper['#attributes']['id'] = 'panopoly-magic-single-preview';
    }

    $response = new AjaxResponse();
    $response->addCommand(new ReplaceCommand($target_selector, $wrapper));
    return $response;
  }

  /**
   * Gets the block add link.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta.
   * @param string $region
   *   The region.
   * @param string $block_id
   *   The block id.
   * @param string $block_label
   *   The block label.
   */
  protected function getBlockAddLink(SectionStorageInterface $section_storage, $delta, $region, $block_id, $block_label) {
    $attributes = $this->getAjaxAttributes();
    $attributes['class'][] = 'js-layout-builder-block-link';
    $add_link = [
      '#type' => 'link',
      '#title' => $this->t('Add<span class="visually-hidden"> @name</span>', ['@name' => $block_label]),
      '#url' => Url::fromRoute('layout_builder.add_block',
        [
          'section_storage_type' => $section_storage->getStorageType(),
          'section_storage' => $section_storage->getStorageId(),
          'delta' => $delta,
          'region' => $region,
          'plugin_id' => $block_id,
        ]
      ),
      '#attributes' => $attributes,
    ];
    return $add_link;
  }

  /**
   * Gets a render array of block links.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   * @param array $blocks
   *   Associative array of block definitions.
   *
   * @return array
   *   The block links render array.
   */
  protected function getBlockLinks(SectionStorageInterface $section_storage, $delta, $region, array $blocks) {
    $contexts = $this->getPopulatedContexts($section_storage);

    $preview_mode = $this->config->get('choose_block_preview');
    $use_descriptions = $this->config->get('choose_block_descriptions');

    $build = [];
    foreach ($blocks as $block_id => $block) {
      // Make the add link.
      $add_link = $this->getBlockAddLink($section_storage, $delta, $region, $block_id, $block['admin_label']);

      $block_description = '';
      if ($use_descriptions) {
        $block_description = $this->blockPreviewRenderer->getBlockDescription($block_id);
      }

      if (in_array($preview_mode, ['automatic', 'manual'])) {
        $block_preview = [
          '#theme' => 'panopoly_magic_preview',
          '#title' => $block['admin_label'],
          '#attached' => [
            'library' => ['panopoly_magic/preview.base'],
          ],
        ];

        if (!empty($block_description)) {
          $block_preview['#title'] = $this->t('@block_name: @block_description', [
            '@block_name' => $block_preview['#title'],
            '@block_description' => $block_description,
          ]);
        }

        if ($preview_mode === 'manual') {
          $block_preview['#attributes']['id'] = 'panopoly-magic-manual-preview-' . Html::getClass($block_id);
          $block_preview['preview'] = [
            '#type' => 'container',
            '#attributes' => [
              'class' => [
                'panopoly-magic-manual-preview-wrapper',
              ],
            ],
          ];
          $block_preview['preview']['link'] = [
            '#type' => 'link',
            '#title' => $this->t('Preview<span class="visually-hidden"> @name</span>', ['@name' => $block['admin_label']]),
            '#url' => Url::fromRoute('panopoly_magic.preview_block', [
              'section_storage_type' => $section_storage->getStorageType(),
              'section_storage' => $section_storage->getStorageId(),
              'delta' => $delta,
              'region' => $region,
              'plugin_id' => $block_id,
            ]),
            '#attributes' => [
              'class' => [
                'use-ajax',
                'button',
                'panopoly-magic-manual-preview-link',
              ],
            ],
          ];
        }
        else {
          $block_preview['preview'] = $this->blockPreviewRenderer->buildBlockPreview($block_id, $contexts);
        }

        $block_preview['add_link'] = $add_link;
        $build[$block_id] = $block_preview;
      }
      // For "single" and "disabled" mode.
      else {
        $block_link = [
          '#theme' => 'panopoly_magic_choose_block_link',
          '#description' => $block_description,
          '#link' => $add_link,
        ];

        // Change the primary link - the $add_link array will remain unchanged.
        $block_link['#link']['#title'] = $block['admin_label'];
        $block_link['#link']['#attributes']['class'][] = 'panopoly-magic-choose-block-link';

        if ($preview_mode === 'single') {
          $block_link['#add_link'] = $add_link;

          // Change the primary link to load the block in the preview.
          $block_link['#link']['#url'] = Url::fromRoute('panopoly_magic.preview_block', [
            'section_storage_type' => $section_storage->getStorageType(),
            'section_storage' => $section_storage->getStorageId(),
            'delta' => $delta,
            'region' => $region,
            'plugin_id' => $block_id,
          ]);
        }

        $build[$block_id] = $block_link;
      }
    }

    return $build;
  }

  /**
   * Get dialog attributes if an ajax request.
   *
   * @return array
   *   The attributes array.
   */
  protected function getAjaxAttributes() {
    if ($this->isAjax()) {
      return [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'dialog',
        'data-dialog-renderer' => 'off_canvas',
      ];
    }
    return [];
  }

}
