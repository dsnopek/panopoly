(function ($, Drupal) {

  $(window).on('dialog:beforecreate', (e, dialog, $element, settings) => {
    if ($element.attr('id') === 'layout-builder-modal') {
      // Use our own logic for getting the dialog buttons.
      settings.buttons = prepareDialogButtons($element);

      // Going forward don't do the Drupal auto buttons thing.
      settings.drupalAutoButtons = false;
    }
  });

  /**
   * Scan a dialog for any primary buttons and move them to the button area.
   *
   * @param {jQuery} $dialog
   *   A jQuery object containing the element that is the dialog target.
   *
   * @return {Array}
   *   An array of buttons that need to be added to the button area.
   */
  function prepareDialogButtons($dialog) {
    const buttons = [];
    const $buttons = $dialog.find(
      '.form-actions input[type=submit], .form-actions a.button',
    );
    $buttons.each(function () {
      const $originalButton = $(this);
      if ($originalButton.hasClass('js-panopoly-magic-live-preview-button')) {
        $originalButton.css({ display: 'none' });
        buttons.push({
          text: $originalButton.html() || $originalButton.attr('value'),
          class: $originalButton.attr('class'),
          click(e) {
            // If the original button is an anchor tag, triggering the "click"
            // event will not simulate a click. Use the click method instead.
            if ($originalButton.is('a')) {
              $originalButton[0].click();
            } else {
              $originalButton
                .trigger('mousedown')
                .trigger('mouseup')
                .trigger('click');
              e.preventDefault();
            }
          },
        });
      }
      else {
        // Show it again, after the drupalAutoButtons thing hid it.
        $originalButton.css({ display: '' });
      }
    });
    return buttons;
  };

  // Copied (and modified) from layout-builder.es6.js
  Drupal.behaviors.panopolyMagicPreviewDisableInteractiveElements = {
    attach() {
      // Disable interactive elements inside preview blocks.
      const $blocks = $('.panopoly-magic-preview-inner');
      $blocks.find('input, textarea, select').prop('disabled', true);
      $blocks
        .find('a')
        // Don't disable the manual preview link.
        .not(
          (index, element) =>
            $(element).hasClass('panopoly-magic-manual-preview-link'),
        )
        .on('click mouseup touchstart', (e) => {
          e.preventDefault();
          e.stopPropagation();
        });

      /*
       * In preview blocks, remove from the tabbing order all input elements
       * and elements specifically assigned a tab index.
       */
      $blocks
        .find(
          'button, [href], input, select, textarea, iframe, [tabindex]:not([tabindex="-1"]):not(.tabbable)',
        )
        // Don't remove the manual preview link from tab order.
        .not(
          (index, element) =>
            $(element).hasClass('panopoly-magic-manual-preview-link'),
        )
        .attr('tabindex', -1);
    }
  };

})(jQuery, Drupal);
