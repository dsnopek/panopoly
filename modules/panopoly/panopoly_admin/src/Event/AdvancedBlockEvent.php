<?php

namespace Drupal\panopoly_admin\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * An event for marking which blocks are considered advanced.
 */
class AdvancedBlockEvent extends Event {

  /**
   * The block definitions.
   *
   * @var array
   */
  protected $definitions;

  /**
   * The blocks marked as advanced.
   *
   * @var array
   */
  protected $advanced = [];

  /**
   * Constructs an AdvancedBlockEvent.
   *
   * @param array $definitions
   *   The block definitions.
   */
  public function __construct(array $definitions) {
    $this->definitions = $definitions;
  }

  /**
   * Gets the block definitions.
   *
   * @return array
   *   An associative array containing the block definitions, keyed by the
   *   plugin id.
   */
  public function getDefinitions() {
    return $this->definitions;
  }

  /**
   * Gets the advanced blocks.
   *
   * @return array
   *   An associative array of the advanced blocks, keyed by the plugin id with
   *   a truthy value.
   */
  public function getAdvanced() {
    return $this->advanced;
  }

  /**
   * Sets the advanced blocks.
   *
   * @param array $advanced
   *   An associative array of the advanced blocks, keyed by the plugin id with
   *   a truthy value.
   */
  public function setAdvanced(array $advanced) {
    $this->advanced = array_filter($advanced);
  }

  /**
   * Checks if the block is marked as advanced.
   *
   * @param string $plugin_id
   *   The plugin id.
   *
   * @return bool
   *   TRUE if the plugin is advanced; otherwise FALSE.
   */
  public function isMarkedAsAdvanced($plugin_id) {
    return isset($this->plugin_id);
  }

  /**
   * Marks an individual block plugin as advanced.
   *
   * @param string $plugin_id
   *   The plugin id.
   * @param bool $value
   *   Whether to mark or unmark the block as advanced.
   */
  public function markAsAdvanced($plugin_id, $value = TRUE) {
    if ($value) {
      $this->advanced[$plugin_id] = TRUE;
    }
    else {
      unset($this->advanced[$plugin_id]);
    }
  }

}
